[Microsoft Protocols ](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-winprotlp/92b33e19-6fff-496b-86c3-d168206f9845)

# Permissions #

To perform special operations, the operating system relies on privileges. They can be displayed by running the command: whoami /all.
- SeLoadDriverPrivilege can be used to take control of the system by loading a specifically designed driver. This procedure can be performed by low privileged users as the driver can be defined in HKCU.
- SeTcbPrivilege is the privilege used to "Act on behalf the operating system". This is the privilege reserved to the SYSTEM user. This procedure allow any users to act as SYSTEM.
- SeDebugPrivilege is the privilege used to debug program and to access any program's memory. It can be used to create a new process and set the parent process to a privileged one.
- SeRestorePrivilege can be used to modify a service running as local system and startable by all users to a chosen one.
- SeBackupPrivilege can be used to backup Windows registry and use third party tools for extracting local NTLM hashes.
- SeTakeOwnershipPrivilege can be used to take ownership of any secureable object in the system including a service registry key. Then to change its ACL to define its own service running as LocalSystem.
- SeCreateTokenPrivilege can be used to create a custom token with all privileges and thus be abused like SeTcbPrivilege
- SeImpersonatePrivilege and SeAssignPrimaryTokenPrivilege can be abused to impersonate privileged tokens. These tokens can be retrieved by establishing security context such as Local DCOM DCE/RPC reflexion.
- SeSecurityPrivilege can be used to clear the security event log and shrink it to make events flushed soon. Also read security log and view events where the user inverted the login and its password.
- SeManageVolumePrivilege can be used to reset the security descriptor on the C volume and thus, change the inherited permissions to critical files

# Privilege escalation

[Windows Privilege Escalation Methods for Pentesters](https://pentest.blog/windows-privilege-escalation-methods-for-pentesters/)

[Priv2Admin - The idea is to "translate" Windows OS privileges to priv escalation](https://github.com/gtworek/Priv2Admin)

# Dump LSASS from a RAM dumping

1. Dump the whole RAM with a forensics tool such as RamCapturer
2. Offline extract lsass from it. Running this in an WSL Ubuntu:

`docker run --rm -it -v "$(pwd)":/app/output --privileged snovvcrash/physmem2profit --mode dump --vmem /app/output/RAM_DUMP.vmem`

- `-v "$(pwd)":/app/output` mounts your current directory (pwd) to /app/output inside the container.
- `--vmem /app/output/RAM_DUMP.vmem` passes the file path inside the container.

## UAC

[UACME](https://github.com/hfiref0x/UACME)


# Credentials gathering
----------------------
https://github.com/AlessandroZ/LaZagne

https://github.com/peewpw/Invoke-WCMDump (it doesn't require local admin privs)

https://github.com/gentilkiwi/mimikatz/wiki/howto-~-credential-manager-saved-credentials (it doesn't require local admin privs)

https://github.com/putterpanda/mimikittenz

[Dumping Clear text credentials](https://pentestlab.blog/2018/04/04/dumping-clear-text-credentials/)

Mimikatz: extracts LSA *secrets* from registry or hive files (lsadump::secrets)

[Invoke-CredentialPhisher] (https://github.com/fox-it/Invoke-CredentialPhisher)


Authentication protocols
====================
[NTLM](ntlm.md)

[Protecting Windows networks - Defeating Pass-The-Hash](https://dfirblog.wordpress.com/2015/11/08/protecting-windows-networks-defeating-pass-the-hash/)

[Practical guide to NTLM Relaying in 2017 (A.K.A getting a foothold in under 5 minutes)](https://byt3bl33d3r.github.io/practical-guide-to-ntlm-relaying-in-2017-aka-getting-a-foothold-in-under-5-minutes.html)



PowerShell
====================
https://artkond.com/2016/12/25/pentesting-windows-powershell/

[Obfuscated Penetration Testing PowerShell scripts](https://github.com/vysec/ps1-toolkit)


Read active sessions remotely
==============================

**Using tasklist.exe**

It requires to run as local admin of the remote host.

Column "User Name" will show the accounts with processes running on the target machine. Truly running proccesses could be filtered using /FI "STATUS eq running"

```
tasklist /V /FI "USERNAME ne NT AUTHORITY\*" /FI "USERNAME ne N/A" /S remote_host
```

Remote credentials can be specified:
```
tasklist /V /FI "USERNAME ne NT AUTHORITY\*" /FI "USERNAME ne N/A" /S remote_host /U domain\username /P password
```

One-liner
TODO: Remove duplicates
```
for /f "tokens=6 delims=," %F in ('tasklist /nh /V /FI "USERNAME ne NT AUTHORITY\*" /S remote_host /fo csv') do @echo %~F
```

**Using PowerShell**

```
ps -IncludeUserName | Select-Object UserName -Unique
```


To run it under a different user's context:

```
$User = "USER"
$Pass = ConvertTo-SecureString -string "SECRET" -AsPlainText –Force
$Cred = New-Object –TypeName System.Management.Automation.PSCredential –ArgumentList $User, $Pass
gwmi win32_process -computername COMPUTER -Credential $Cred | Select-Object @{n='Owner';e={$_.GetOwner().User}} -Unique
```

AppLocker
=========
https://github.com/api0cradle/UltimateAppLockerByPassList


Post Exploitation
===================
[Post Exploitation in Windows using dir Command](http://www.hackingarticles.in/post-exploitation-windows-using-dir-command/)

[Post Exploitation Using WMIC (System Command)](http://www.hackingarticles.in/post-exploitation-using-wmic-system-command/)


Get Reverse-shell via Windows one-liner
====================
[Get Reverse-shell via Windows one-liner](https://www.hackingarticles.in/get-reverse-shell-via-windows-one-liner/)

[Windows oneliners to download remote payload and execute arbitrary code](https://arno0x0x.wordpress.com/2017/11/20/windows-oneliners-to-download-remote-payload-and-execute-arbitrary-code/)


Persistence
===================
[Ever Present Persistence Established Footholds Seen in the Wild](https://www.youtube.com/watch?v=YP6-8pWqYhw)

[Windows Userland Persistence Fundamentals](http://www.fuzzysecurity.com/tutorials/19.html)

[Persistence techniques on Pentestlab](https://pentestlab.blog/methodologies/red-teaming/persistence/)


Crack NT hashes from LM cracked-hashes
=========================================

https://gist.github.com/jesux/2a851ab195ab1e02c1ddab5e508aec77

Using John Jumbo edition: 

https://malicious.link/post/2012/2012-10-24-lm2ntlm-with-john-the-ripper/

https://blog.didierstevens.com/2016/07/21/practice-ntds-dit-file-part-8-password-cracking-with-john-the-ripper-lm-ntlm/


[A tool for automating cracking methodologies through Hashcat](https://github.com/trustedsec/hate_crack)



Covering tracks
===============
[Clear Windows log](https://twitter.com/gentilkiwi/status/943262457287634944)

[Phant0m: Killing Windows Event Log](https://artofpwn.com/phant0m-killing-windows-event-log.html). With #Phant0m the Windows Event #Log Service of the target system will not be able to collect logs. At the same time will appear to be running because only the related threads will be killed.


Threat Hunting
===============
[Hunting for Privilege Escalation in Windows Environment](https://speakerdeck.com/heirhabarov/hunting-for-privilege-escalation-in-windows-environment)


Others
====================
https://attackerkb.com/Windows/blind_files

https://twitter.com/DirectoryRanger

https://www.hackingarticles.in/windows-for-pentester-certutil/


