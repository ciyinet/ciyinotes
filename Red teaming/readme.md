https://dmcxblue.gitbook.io/red-team-notes/

[Red Teaming/Adversary Simulation Toolkit](https://github.com/infosecn1nja/Red-Teaming-Toolkit/blob/master/README.md)

[Planning a Red Team exercise](https://github.com/vysec/redteam-plan/blob/master/README.md)

[Red Team infrastructure](https://github.com/bluscreenofjeff/Red-Team-Infrastructure-Wiki)

[6 RED TEAM INFRASTRUCTURE TIPS](https://cybersyndicates.com/2016/11/top-red-team-tips/)

[A collection of Red team infrastructure and operations resources](https://www.peerlyst.com/posts/a-collection-of-red-team-infrastructure-and-operations-resources-claus-cramon?utm_source=twitter&utm_medium=social&utm_content=peerlyst_post&utm_campaign=peerlyst_resources)

Red Team: How to Succeed By Thinking Like the Enemy

[Red Teamers Can Learn Secrets by Purple Teaming](https://www.alienvault.com/blogs/security-essentials/red-teamers-can-learn-secrets-by-purple-teaming?utm_medium=Social&utm_source=Twitter&utm_content=Awareness)

[BSides Columbus08 Planning and Executing a Red Team Engagement Timothy Wright](https://www.youtube.com/watch?v=__uo8Qs9FIA&feature=youtu.be)

[A Red Teamer's guide to pivoting](https://artkond.com/2017/03/23/pivoting-guide/)

[Advanced Threat Tactics – Course and Notes](https://blog.cobaltstrike.com/2015/09/30/advanced-threat-tactics-course-and-notes/)

[Red tips](https://github.com/vysec/RedTips) Via [(Twitter)](https://twitter.com/vysecurity)

[Top Five Ways the Red Team breached the External Perimeter](https://medium.com/@adam.toscher/top-five-ways-the-red-team-breached-the-external-perimeter-262f99dc9d17)

[Tools to gather emails for an organization](https://twitter.com/Jhaddix/status/1118614418462367744)


**Microsoft**

[MS Just Gave the Blue Team Tactical Nukes (And How Red Teams Need To Adapt) ](https://media.defcon.org/DEF%20CON%2025/DEF%20CON%2025%20presentations/DEFCON-25-Chris-Thompson-MS-Just-Gave-The-Blue-Teams-Tactical-Nukes.pdf)


PowerShell
===========
Alternatives to PowerShell (These are useful when powershell.exe is blocked)
- [LyncAppvPublishingServer](https://twitter.com/monoxgas/status/895045566090010624/photo/1)
- [PowerShdll - Run PowerShell with rundll32 (Bypass software restrictions)](https://github.com/p3nt4/PowerShdll)
- [PowerLessShell](https://github.com/Mr-Un1k0d3r/PowerLessShell)

Windows PowerShell version 1 does not support having its console output redirected, but PowerShell version 2 and newer does if started with the –File – command-line option— 

To have an interactive PowerShell session with PsExec:

`psexec \\server1 PowerShell.exe -file - `

For all operations on remote computers, PsExec extracts an EXE file to the Windows directory of the target computer and registers it as a service. By default, the file name is PSEXESVC.exe and the service name is PSEXESVC. Both can be changed with the –r servicename option. 

For example to use file name WmiPrvSys.exe and service name WmiPrvSys:

`psexec –r WmiPrvSys \\server1 cmd.exe`


[Download Cradles](https://gist.github.com/HarmJ0y/bb48307ffa663256e239)

[Invoke-CradleCrafter](https://github.com/danielbohannon/Invoke-CradleCrafter)


Phishing
==========
[tips on bypassing 0365 filters for phishing](https://twitter.com/byt3bl33d3r/status/895333432401608704?s=09)

[Malicious Macro Generator Utility](https://github.com/Mr-Un1k0d3r/MaliciousMacroGenerator)

[Macro Creator](https://github.com/Arno0x/PowerShellScripts/tree/master/MacroCreator)

[How to bypass email gateways using common payloads... Bsides Manchester 2017](https://www.slideshare.net/NeilLines1/how-to-bypass-email-gateways-using-common-payloads-bsides-manchester-2018)

[EMPIRE: Multi-Platform Macro Phishing Payloads](https://medium.com/@malcomvetter/multi-platform-macro-phishing-payloads-3b688e8eff68)

Post-Exploitation Frameworks
==========================
**Cobalt Strike**
[morphHTA - Morphing Cobalt Strike PowerShell Evil HTA Generator](http://www.kitploit.com/2017/06/morphhta-morphing-cobalt-strike.html)

**Empire**
[Official website](http://www.powershellempire.com/)


https://twitter.com/h4knet/status/1110524125964128258

https://www.slideshare.net/JasonLang1/modern-evasion-techniques?next_slideshow=1
