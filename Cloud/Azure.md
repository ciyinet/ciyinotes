Monkey365
----------------
**Monkey365**

[Download](https://github.com/silverhack/monkey365)

Follow instructions to install. 

Azure Security Audit. PowerShell:

```
$param = @{
     Instance = 'Azure';
	 Analysis = 'All';
     PromptBehavior = 'SelectAccount';
	 subscriptions = '00000000-0000-0000-0000-000000000000 11111111-1111-1111-1111-111111111111';
     TenantID = '00000000-0000-0000-0000-000000000000';
     IncludeEntraID = $true;
     ExportTo = @("CLIXML","EXCEL","CSV","JSON","HTML");
}     
$assets = Invoke-Monkey365 @param
```


```
$param = @{
    Instance = 'Azure';
	Analysis = 'All';
    PromptBehavior = 'SelectAccount';
    AllSubscriptions = $true;
    TenantID = '00000000-0000-0000-0000-000000000000';
	IncludeEntraID = $true;
    ExportTo = @("CLIXML","EXCEL","CSV","JSON","HTML");
}
$assets = Invoke-Monkey365 @param
```

Microsoft365 Security Audit. PowerShell:


```
$param = @{
    Instance = 'Microsoft365';
    Analysis = @("ExchangeOnline","SharePointOnline","Purview","MicrosoftTeams","Microsoft365");
    PromptBehavior = 'SelectAccount';
    IncludeEntraID = $true;
    ExportTo = @("CLIXML","EXCEL","CSV","JSON","HTML");
	OutDir = 'C:\Users\tester\Desktop\Tools\Azure\monkey-reports';
}
Invoke-Monkey365 @param
```



(To Export to excel it's required ot have Excel installed)

The reports will be under monkey-reports in the monkey365 folder.




AzureHound
--------------------

Dealing with Multi-Factor Auth and Conditional Access Policies [https://support.bloodhoundenterprise.io/hc/en-us/articles/17481394564251-AzureHound-Community-Edition#heading-2]


From on-prem AD admin to Azure
-------------------------------

https://aadinternals.com/post/on-prem_admin/

If ADFS is in used: https://aadinternals.com/post/adfs/

Once ADFS is compromised, one can also use the tokens to use typical Ms PowerShell modules. For instance MSOnline:

```
$saml_token = New-AADIntSAMLToken -ImmutableID <ID> -PfxFileName C:\Users\tester\Desktop\ADFS_signing.pfx -Issuer http://ISSUER_URI/adfs/services/trust/
$token_ad = Get-AADIntAccessTokenForAADGraph -SAMLToken $saml_token
$token_ms = Get-AADIntAccessTokenForMSGraph -SAMLToken $saml_token
Connect-MsolService -AdGraphAccessToken $token_ad -MsGraphAccessToken $token_ms
```



Get members of groups named like
--------------------------------
```
$groups = Get-AzureADGroup -All $true | Where-Object {$_.DisplayName -like "*MFA*Exceptions*"}
foreach ($group in $groups) { Get-AzureADGroupMember -ObjectId $group.objectId -All $true | select DisplayName, UserPrincipalName, @{Name="Azure Group";Expression={ $group.DisplayName }} }
```

Get users that must reset their pwd on next level
-------------------------------------------------
```
Get-AzureADUser -All $true | select mail,mailnickname,@{Name="Must reset pwd";Expression={ $_.passwordprofile.ForceChangePasswordNextLogin }} | Export-Csv pwd_reset_next_login.csv
```

Search for all the site you have access to in SharePoint Online
----------------------------

`contentclass:STS_Site`

Source: https://support.4it.com.au/article/how-to-show-all-sites-you-have-access-to-in-sharepoint-online/


Share a subscription with another Tenant
-------------------------------------------
Instructions to share a subscription from tenant B with a different tenant A and change the owner from user "OwnB" to "OwnA". 

1. Add the owner "OwnB" of the subscription of tenant B as a guest user in tenant A.
1. Temporarily make "OwnB" a Global Admin of Tenant A (NOTE: To be reviewed this step to use a lower-priv role in the future.)
1. Login as owner "OwnB" in TenantB. Go to subscription -> Change directory -> select the tenant B.
1. Still logged in as "OwnB", switch directory to TenantA.
1. Go to the subscription. Then use subscription IAM and add a role (Owner) to the admin of the tenant A ("OwnA"). This way, from now on the standard admin in Tenant A ("OwnA") can manage the subscription.
1. Remove Global Admin role from the "OwnB" in TenantA.


Other links
-----------

[o365 Blog](https://o365blog.com/)

[Getting Started in Pentesting The Cloud–Azure | Beau Bullock | 1-Hour](https://www.youtube.com/watch?v=u_3cV0pzptY)

[Azure-Red-Team](https://github.com/rootsecdev/Azure-Red-Team)

https://github.com/Cloud-Architekt/AzureAD-Attack-Defense

Azure abuses: https://blog.netspi.com/author/karl-fosaaen/

https://github.com/appsecco/CloudPentestCheatsheets/blob/master/cheatsheets/Azure.md

[Office 365 Attacks](https://onedrive.live.com/view.aspx?resid=F32A9F4F1477E49!122&ithint=file%2cpptx&authkey=!ACC5Ztb5uVED22k)

[Owning O365 Through Better Brute-Forcing](https://www.trustedsec.com/blog/owning-o365-through-better-brute-forcing/)

[Email reconnaissance with no login attempts](https://github.com/Raikia/UhOh365)

https://github.com/mdsecactivebreach/o365-attack-toolkit

[MSOLSpray](https://github.com/dafthack/MSOLSpray)

[Security best practices for Azure solutions](https://azure.microsoft.com/mediahandler/files/resourcefiles/security-best-practices-for-azure-solutions/Azure%20Security%20Best%20Practices.pdf)

https://www.hub.trimarcsecurity.com/post/webcast-securing-office-365-and-azure-ad-defend-your-tenant

[Introducing ROADtools - The Azure AD exploration framework ](https://dirkjanm.io/introducing-roadtools-and-roadrecon-azure-ad-exploration-framework/)

[AAD Killchain](https://o365blog.com/aadkillchain/)

https://www.youtube.com/watch?v=fpUZJxFK72k

https://www.microsoft.com/security/blog/2021/06/29/mitre-attck-mappings-released-for-built-in-azure-security-controls/

[AzureStealth - Discover the most privileged users in Azure and secure\target them](https://github.com/cyberark/SkyArk/blob/master/AzureStealth/AzureStealth.ps1)
