Resources that could help gain some practical experience:

## Active Directory ##

[Detection Lab](https://medium.com/@clong/introducing-detection-lab-61db34bed6ae)



## Web ##
[OWASP Mutillidae](http://sourceforge.net/projects/mutillidae/)

**Description:** OWASP Mutillidae II is a free, open source, deliberately vulnerable web-application providing a target for web-security enthusiest. Mutillidae can be installed on Linux and Windows using LAMP, WAMP, and XAMMP. It is pre-installed on SamuraiWTF, Rapid7 Metasploitable-2, and OWASP BWA. The existing version can be updated on these platforms. With dozens of vulns and hints to help the user; this is an easy-to-use web hacking environment designed for labs, security enthusiast, classrooms, CTF, and vulnerability assessment tool targets. Mutillidae has been used in graduate security courses, corporate web sec training courses, and as an "assess the assessor" target for vulnerability assessment software.


[bWAPP](http://www.mmeit.be/bWAPP/)

**Description:** bWAPP, or a buggy web application, is a free and open source deliberately insecure web application. It helps security enthusiasts, developers and students to discover and to prevent web vulnerabilities. bWAPP prepares one to conduct successful penetration testing and ethical hacking projects.


[OWASP Broken Web Applications Project](https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project)

**Description:** The Broken Web Applications (BWA) Project produces a Virtual Machine running a variety of applications with known vulnerabilities for those interested in:
learning about web application security
testing manual assessment techniques
testing automated tools
testing source code analysis tools
observing web attacks
testing WAFs and similar code technologies

## Mobile ##
[Damn Vulnerable Web Application (DVWA)](http://www.dvwa.co.uk/)

**Description:** Damn Vulnerable Web App (DVWA) is a PHP/MySQL web application that is damn vulnerable. Its main goals are to be an aid for security professionals to test their skills and tools in a legal environment, help web developers better understand the processes of securing web applications and aid teachers/students to teach/learn web application security in a class room environment.


[Damn Vulnerable iOS Application (DVIA)](http://damnvulnerableiosapp.com/)

**Description:** Damn Vulnerable iOS App (DVIA) is an iOS application that is damn vulnerable. Its main goal is to provide a platform to mobile security enthusiasts/professionals or students to test their iOS penetration testing skills in a legal environment.


## Infrastructure ##
[Damn Vulnerable Linux](http://distrowatch.com/table.php?distribution=dvl)

**Description:** Damn Vulnerable Linux (DVL) is a Slackware and Slax-based live DVD. The distribution, purposefully stuffed with broken, ill-configured, outdated and exploitable software, began life as a training system used during the author's university lectures.


[Metasploitable](https://information.rapid7.com/metasploitable-download.html)

**Description:** Taking your first steps with Metasploit can be difficult – especially if you don’t want to conduct your first penetration test on your production network. Metasploitable is virtual machine based on Linux that contains several intentional vulnerabilities for you to exploit. Metasploitable is essentially a penetration testing lab in a box, available as a VMware virtual machine (VMX). (The Metasploitable login is “msfadmin”; the password is also “msfadmin”.)

https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project



### CTF platforms ###
https://www.root-me.org/en/Challenges/

https://www.vulnhub.com/

https://www.hackthissite.org/

http://fuzyll.com/2016/the-defcon-ctf-vm/

https://www.wechall.net/en/

https://trailofbits.github.io/ctf/index.html

http://www.hackingarticles.in/capture-flag-challenges

https://www.hackthebox.eu/ (Similar boxes as OSCP)

https://lab.pentestit.ru/

https://www.hackthis.co.uk/
