**Official documentation**

https://bloodhound.readthedocs.io/en/latest/



You can see any errors reported by the GUI in the developer console with Ctrl+shift+i


![](images/need_admin_rigths.png)




Custom Cypher queries
==========================

These can be executed directly at http://localhost:7474/browser/ 


**Hosts configured for unconstrained delegation**

`
MATCH (c:Computer {unconstraineddelegation:true})
RETURN c.name
`

**non-domain-controllers w/ unconstrained delegation & where they exist in your domain(s) OU tree**

`
MATCH (dc:Computer)-[:MemberOf*1..]->(g:Group)
WHERE g.objectsid ENDS WITH "516"
WITH COLLECT(dc) as domainControllers
MATCH p = (d:Domain)-[:Contains*1..]->(c:Computer {unconstraineddelegation:true})
WHERE NOT c in domainControllers
RETURN COUNT(p)
`

**Stats DA Accounts with Sessions on Non DC Machines**

`
OPTIONAL MATCH (c:Computer)-[:MemberOf]->(t:Group) 
WHERE NOT t.name =~ 'DOMAIN CONTROLLERS@.*'
WITH c as NonDC
MATCH p=(NonDC)-[:HasSession]->(n:User)-[:MemberOf]->(g:Group) WHERE g.name =~ 'DOMAIN ADMINS@.*'
RETURN DISTINCT (n.name) as Username, COUNT(DISTINCT(NonDC)) as Connexions
ORDER BY COUNT(DISTINCT(NonDC)) DESC
`

**DA Accounts with Sessions on Non DC Machines**

``
OPTIONAL MATCH (c:Computer)-[:MemberOf]->(t:Group) 
WHERE NOT t.name =~ 'DOMAIN CONTROLLERS@.*|ENTERPRISE DOMAIN CONTROLLERS@.*'
WITH c as NonDC
MATCH p=(NonDC)-[:HasSession]->(n:User)-[:MemberOf]->(g:Group) WHERE g.name =~ 'DOMAIN ADMINS@.*|ENTERPRISE ADMINS@.*|ADMINISTRATORS@.*|'
RETURN n.name AS `Privileged user with an active session`, NonDC.name AS `Non-DC servers where the session is active`, g.name AS `User’s domain group (among others)`
``

**Service accounts with excessive permissions (members of privileged groups)**

`
MATCH (n:User)-[:MemberOf]->(g:Group) WHERE g.name =~ 'DOMAIN ADMINS@.*|ENTERPRISE ADMINS@.*|ADMINISTRATORS@.*|' AND n.hasspn=true
RETURN n.name, g.name, n.pwdlastset, n.pwdneverexpires
`

**non-DAs that can control any DA and how**

`
MATCH p1 = (daPrincipal)-[:MemberOf*1..]->(daGroup:Group {name:'DOMAIN ADMINS@DOMAIN.COM'})
WITH p1,daPrincipal,daGroup
OPTIONAL MATCH p2 = (l)-[{isacl:true}]->(daPrincipal)
WHERE NOT l = daGroup AND NOT l = daPrincipal
OPTIONAL MATCH p3 = (m)-[:MemberOf*1..]->(g:Group)-[{isacl:true}]->(daPrincipal)
WHERE NOT m = daPrincipal AND NOT m = daGroup AND NOT g = daGroup AND NOT (m)-[:MemberOf*1..]->(daGroup)
RETURN p1,p2,p3
`

**non-DAs that can control any GPO that applies to any DA and how**

Find the GPOs that apply to DA:

`
MATCH (g3:Group {name:'DOMAIN ADMINS@DOMAIN.COM'})
OPTIONAL MATCH p1 = (g1:GPO)-[:GpLink {enforced:false}]->(container)-[:Contains*1..]->(u1:User)-[:MemberOf*1..]->(g3)
WHERE NONE (x in NODES(p1) WHERE x.blocksinheritance = true AND x:OU AND NOT (g1)-->(x))
OPTIONAL MATCH p2 = (g2:GPO)-[:GpLink {enforced:true}]->(container)-[:Contains*1..]->(u2:User)-[:MemberOf*1..]->(g3)
RETURN p1,p2
`

From the previous list, this will show who (excluding Domain Admins) controls those GPOs :

`
MATCH (g3:Group {name:'DOMAIN ADMINS@DOMAIN.COM'})
OPTIONAL MATCH p1 = (g1:GPO)-[:GpLink {enforced:false}]->(container)-[:Contains*1..]->(u1:User)-[:MemberOf*1..]->(g3)
WHERE NONE (x in NODES(p1) WHERE x.blocksinheritance = true AND x:OU AND NOT (g1)-->(x))
WITH p1,g1,g3,u1
OPTIONAL MATCH p2 = (g2:GPO)-[:GpLink {enforced:true}]->(container)-[:Contains*1..]->(u2:User)-[:MemberOf*1..]->(g3)
WITH p1,p2,g1,g2,g3,COLLECT(u1) + COLLECT(u2) as u
OPTIONAL MATCH p3 = (l)-[{isacl:true}]->(g1)
WHERE NOT l = g3
OPTIONAL MATCH p4 = (m)-[:MemberOf*1..]->(g4:Group)-[{isacl:true}]->(g1)
WHERE NOT (m)-[:MemberOf*1..]->(g3) AND NOT m = g3
OPTIONAL MATCH p5 = (n)-[{isacl:true}]->(g2)
WHERE NOT n = g3
OPTIONAL MATCH p6 = (o)-[:MemberOf*1..]->(g5:Group)-[{isacl:true}]->(g2)
WHERE NOT o IN u AND NOT o = g3 AND NOT g5 = g3
RETURN p1,p2,p3,p4,p5,p6
`



**List all explicit foreign admins for computers in DOMAINA.LOCAL along with the computer they are AdminTo**

`
MATCH (u:User) -[AdminTo]-> (c:Computer {domain: 'DOMAINA.LOCAL'})
WHERE NOT u.domain = 'DOMAINA.LOCAL'
RETURN u.name, c.name
`

**List all computers in DomainA where users from DomainB are explicitly allowed local admin access**

`
MATCH (u:User {domain: 'DOMAINB.LOCAL'})-[r:AdminTo]->(c:Computer {domain: 'DOMAINA.LOCAL'})
RETURN u.name, c.name
`

**Computers that Domain Users have AllExtendedRights over**

`
MATCH (g:Group) -[r:AllExtendedRights]-> (c:Computer) WHERE g.name =~ 'DOMAIN USERS@.*' RETURN c.name
`

**List users and groups that are Owners of a Domain Controller**


`
MATCH (n)-[r:Owns]-> (c:Computer) -[:MemberOf]-> (g:Group)  WHERE (n:User or n:Group) AND g.name =~ '.*DOMAIN CONTROLLERS.*' RETURN labels(n), n.name, c.name, g.name
`

**Users or Group that are not part of Domain Admins and can RDP to (some) Domain Controllers**

`
MATCH (n) -[:MemberOf]-> (g1:Group) -[r:CanRDP]-> (c:Computer) -[:MemberOf]-> (g2:Group)  WHERE (n:User or n:Group) AND NOT g1.name =~ '.*DOMAIN ADMINS@.*' AND g2.name =~ 'DOMAIN CONTROLLERS@.*' RETURN labels(n), n.name, c.name, g2.name
`

**Domain Admins with active sessions on Non (Enterprise\|Read-Only) Domain Controllers machines**

`
OPTIONAL MATCH (c:Computer)-[:MemberOf]->(t:Group) 
WHERE NOT t.name =~ '.*DOMAIN CONTROLLERS@.*'
WITH c as NonDC
MATCH p=(NonDC)-[:HasSession]->(n:User)-[:MemberOf]-> (g:Group)
WHERE g.name =~ 'DOMAIN ADMINS@.*'
RETURN DISTINCT (n.name) as Username, COUNT(DISTINCT(NonDC)) as Connexions
ORDER BY COUNT(DISTINCT(NonDC)) DESC
`

** Find interesting permission a group have over other objects in the domain. other permissions could be added.**

`
MATCH (g:Group) -[r:Owns|GenericAll|GenericWrite|WriteOwner|WriteDacl|ForceChangePassword|AllExtendedRights|AddMember|Contains|GPLink|AllowedToDelegate|TrustedBy|AllowedToAct|AdminTo|CanPSRemote|CanRDP|ExecuteDCOM|HasSIDHistory|AddSelf|DCSync|ReadLAPSPassword|ReadGMSAPassword|DumpSMSAPassword|SQLAdmin|AddAllowedToAct|WriteSPN|AddKeyCredentialLink|SyncLAPSPassword|WriteAccountRestrictions|GoldenCert|ADCSESC1|ADCSESC3|ADCSESC4|ADCSESC5|ADCSESC6a|ADCSESC6b|ADCSESC7|ADCSESC9a|ADCSESC9b|ADCSESC10a|ADCSESC10b|DCFor]-> (c)  WHERE g.name =~ 'DOMAIN USERS@.*|AUTHENTICATED USERS@.*|USERS@.*|DOMAIN GUESTS@.*|GUESTS@.*|DOMAIN COMPUTERS@.*' RETURN g.name, type(r), c.name, labels(c)
`

** Find Owners of Domain Controllers and Enterprise Domain Controllers **

`
MATCH (e) -[r:Owns]->(c:Computer)-[:MemberOf]->(t:Group)  WHERE t.name =~ 'DOMAIN CONTROLLERS@.*' OR t.name =~ 'ENTERPRISE DOMAIN CONTROLLERS@.*' RETURN e.name as Owner, labels(e) as OwnerType, c.name as DCName, t.name as DCGroup
`

** Find all permissions that a User, Group or Computer has **

`
MATCH (g) -[r]-> (c)  WHERE (g:User or g:Group or g:Computer)  RETURN g.name, labels(g), type(r), c.name, labels(c)
`

`
MATCH (g:Group) -[r]-> (c) WHERE g.name =~ 'DOMAIN USERS@.*|AUTHENTICATED USERS@.*|USERS@.*|EVERYONE@.*|DOMAIN GUESTS@.*|GUESTS@.*|DOMAIN COMPUTERS@.*' RETURN g.name, labels(g), type(r), c.name, labels(c)
`

`
MATCH (g) -[r]-> (c) WHERE c.name =~ 'DOMAIN ADMINS@.*|ENTERPRISE ADMINS@.*|ACCOUNT OPERATORS@.*|ADMINISTRATORS@.*|DNSADMINS@.*|DOMAIN CONTROLLERS@.*|GROUP POLICY CREATOR OWNERS@.*|ADMINISTRATOR@.*|KRBTGT@.*|PRINT OPERATORS@.*|READ ONLY DOMAIN CONTROLLERS@.*|SCHEMA ADMINS@.*|SERVER OPERATORS@.*' RETURN g.name, labels(g), type(r), c.name, labels(c)
`

*Don't forget to also check for the domain object.*

`
MATCH (a) -[r]-> (d:Domain) RETURN a.name, labels(a), type(r)
`

Add the relationship AdminTo from USER-1 to every computer affected by the GPO GPO-ABC:

```
MATCH (g:GPO) WHERE g.name = 'GPO-ABC' WITH g
OPTIONAL MATCH (g)-[r1:GPLink {enforced:false}]->(container1) WITH g,container1 
   OPTIONAL MATCH (g)-[r2:GPLink {enforced:true}]->(container2) WITH g,container1,container2
OPTIONAL MATCH p1 = (g)-[r1:GPLink]->(container1)-[r2:Contains*1..]->(n1:Computer) WHERE NONE(x in NODES(p1) WHERE x.blocksinheritance = true AND LABELS(x) = 'OU') WITH g,container2,collect(n1) AS computers1 
OPTIONAL MATCH p2 = (g)-[r1:GPLink]->(container2)-[r2:Contains*1..]->(n2:Computer) WITH computers1 + collect(n2) AS computers
	
// Adding relationships between A and each computer in the list
MATCH (a:User {name: 'USER-1'})
FOREACH (computer IN computers | 
	MERGE (a)-[:AdminTo]->(computer)
)
```

Extra info on https://github.com/RiccardoAncarani/GPOPowerParser


More on 

[DogWhisperer’s SharpHound Cheat Sheet](https://insinuator.net/2021/05/dogwhisperers-sharphound-cheat-sheet/)

[BloodHound Tips and Tricks](https://www.riccardoancarani.it/bloodhound-tips-and-tricks/)

https://github.com/seajaysec/cypheroth

https://github.com/awsmhacks/awsmBloodhoundCustomQueries#shortest-path-from-domain-users-to-domain-admins

https://hausec.com/2019/09/09/bloodhound-cypher-cheatsheet/

https://bloodhoundhq.slack.com/messages/C9U6W5TA6/

https://github.com/ZephrFish/Bloodhound-CustomQueries



PlumHound
===================
[PlumHound](https://github.com/DefensiveOrigins/PlumHound)

Run default tests: 

`python3 PlumHound.py -s bolt://localhost:7687 -u neo4j -p Bloodhound -x tasks/default.tasks`



AzureHound
===================

**Check for on-prem user with a path to your Azure Tenant**

`
match (x:User)-[:AZPrivilegedRoleAdmin|AZGlobalAdmin]->(t:AZTenant) with collect(x) as adm, t match p=shortestpath((n:User)-[*1..]->(t)) where and not n in adm return p
`

