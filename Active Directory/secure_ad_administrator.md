- Perform your Windows administration from a "secure workstation"
    - Clean sourced. Current OS. Patched
    - No Internet or email access
    - Establish Identity Assurance (MFA)
- User Protected Users




[Securing Privileged Access](https://docs.microsoft.com/en-us/windows-server/identity/securing-privileged-access/securing-privileged-access)

[Best Practices for Securing Active Directory](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/best-practices-for-securing-active-directory)

[Awesome Windows Domain Hardening](https://github.com/PaulSec/awesome-windows-domain-hardening)

https://www.secframe.com/blog/escalation-defenses-ad-guardrails-every-company-should-deploy


https://github.com/davidprowe/AD_Sec_Tools/tree/master/Presentations

[PlumHound - BloodHoundAD Report Engine for Security Teams](https://github.com/DefensiveOrigins/PlumHound)