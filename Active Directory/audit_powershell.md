Get recursively admin users without the flag "Account is sensitive and cannot be delegated":

`foreach ($user in Get-DomainGroupMember -Server <DC> -Recurse -Identity Administrators | Where-Object { $_.MemberObjectClass -Match 'user'} | Select-Object membername | Get-Unique) { Get-DomainUser -server <DC> -Identity $user.membername -UACFilter NOT_TRUSTED_FOR_DELEGATION -Properties distinguishedname,samaccountname | Export-Csv admin_not_Deleg.csv -Append }`


Get Non-DC with Unconstrained delegation

`Get-DomainComputer -Unconstrained -Server <DC> -Properties dnshostname,distinguishedname,operatingsystem | Where-Object {$_.dnshostname -notIn (Get-DomainController -Server <DC> | Select-Object dnshostname).dnshostname} | Export-Csv non_dc_unconstrained.csv`