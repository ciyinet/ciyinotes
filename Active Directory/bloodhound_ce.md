Installation on Windows
---------------

https://support.bloodhoundenterprise.io/hc/en-us/articles/17468450058267-Install-BloodHound-Community-Edition-with-Docker-Compose

https://support.bloodhoundenterprise.io/hc/en-us/articles/17481394564251-AzureHound-Community-Edition

Once installed:

- Bloohound GUI: http://localhost:8080/
- Neo4j webconsole: http://localhost:7474


Clean database
-----------------
Go to the same folder where Bloodhound CE is (and the docker-compose.yml is) and run in PowerShell (as admin):

`docker compose down -v`

And then:

`docker compose up`

If you need to restart entirely, you can run `docker compose down -v`. This will also delete the attached volumes associated with the containers in your docker compose file. A follow-up docker compose up will recreate all new images and volumes, and you'll start fresh.

**If you just want to delete the data contained in Neo4J**, but leave the configuration of users, etc in place:

Run to shut down the running stack

`docker compose down`

Run one of the following based on your architecture:

For Bash compatible shells: `docker volume rm $(docker volume ls -q | grep neo4j-data)`

For PowerShell: `docker volume rm @(docker volume ls -q | Select-String neo4j-data)`

Then:

`docker compose up`