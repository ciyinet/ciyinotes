# Reconnaissance
These commands are built-in/native in Windows.

**Command** | **Explanation**
--------- | ---
echo %USERDOMAIN% <br/><br/>echo %USERDNSDOMAIN%<br/><br/>ipconfig /all  | Obtain domain name
echo %logonserver% | It prints hostname of current primary DC in use
set logon | It prints hostname of current primary DC in use
net user \<USERNAME\> /domain | Obtain AD information of \<USERNAME\> domain user
net users /domain | List all users within the domain
net view /domain:\<DOMAIN\> | TBC
net group "domain computers" /domain | Lists computers in the domain
nltest /dclist:\<DOMAIN\> <br/><br/>net group "Domain Controllers" /domain | It lists the Domain Controllers of the domain \<DOMAIN\>
net group /domain | It gives all the groups within the domain
net localgroup "Administrators" /domain | List of users and group of users that are part of "Administrators" built-in group
net group "Domain Admins" /domain | It gives the list of users that belong to the group "Domain Admins"
net accounts /domain | Obtain current domain password policy 
nltest /domain_trusts [/PRIMARY /FOREST /DIRECT_OUT /DIRECT_IN /ALL_TRUSTS /V] | Show trusts relationship
netdom query /domain \<DOMAIN\> TRUST | Show trusts relationship
nltest /dsgetdc:\<DomainDNSName> [/site:\<SiteName\>] [/server:\<ClientName\>] | Finds the closest domain controller in the specified domain(\<DomainDNSName\>); that is, a domain controller that is located in the same site or in theclosest site if a local DC is not available. By default, it will return the closest DC for thecomputer from which nltest is being run, but you can optionally use the /server optionto target a remote host. If you are interested in finding a DC within a particular siteregardless  of  whether  it  is  the  closest  DC  to  you,  you  can  also  optionally  specifythe /site option to find a domain controller that belongs to a particular site
nltest /dcname:\<DOMAIN\> | Gets the primary domain controller for \<DOMAIN\>
findstr /S /I cpassword \<FQDN>\sysvol<FQDN>\policies\ *.xml | Find GPP passwords in sysvol

**PowerView**

[PowerView (PowerSploit)](https://github.com/PowerShellMafia/PowerSploit/tree/master/Recon) (The latest version is always on [Dev branch](https://github.com/PowerShellMafia/PowerSploit/tree/dev/Recon))

[Updated version of PowerView](https://exploit.ph/powerview.html)

**Command** | **Explanation**
--------- | ---
Get-DomainGroupMember -Identity Administrators -Recurse | Get recursively all members (including group and computers) of built-in Administrators group (Admins of the domain)
foreach ($user in Get-DomainGroupMember -Server \<DC\> -Recurse -Identity Administrators &#124; Where-Object { $\_.MemberObjectClass -Match 'user'} &#124; Select-Object membername &#124; Get-Unique) { Get-DomainUser -server <\DC\> -Identity $user.membername -UACFilter NOT_TRUSTED_FOR_DELEGATION -Properties distinguishedname,samaccountname &#124; Export-Csv admin_not_Deleg.csv -Append } | Get recursively admin users without the flag "Account is sensitive and cannot be delegated"
Get-DomainUser -AdminCount &#124; select samaccountname | Get Admins of the domain
Get-DomainGroup &#124; ? {$_.samaccountname -like '\*PATTERN\*' } | List Groups that contains a PATTERN within their name
Invoke-ACLScanner -ResolveGUIDs &#124; Where { ($\_.ObjectDN -eq 'dc=lab,dc=adsecurity,dc=org') -AND ($_.ObjectType -match "Replicat") } | Identify groups/accounts with **DCSync** rights. Match up accounts that have both DS-Replication-Get-Changes & DS-Replication-Get-Changes-All rights
Set-DomainUserPassword -Identity USERNAME -AccountPassword (ConvertTo-SecureString -AsPlainText "Passw0rd!!" -Force) | Reset a user's password
$userpass = ConvertTo-SecureString 'Passw0rd!!' -AsPlainText -Force; $creds = New-Object System.Management.Automation.PsCredential('DOMAIN\USERNAME', $userpass); Add-DomainGroupMember -Identity 'GROUP' -Members 'USERNAME2' -Credential $creds | Add a member to a group using someone's else credentials that has that privilege
Get-DomainUser -UACFilter DONT_EXPIRE_PASSWORD | Find users with non-expiring passwords. Other values at: https://support.microsoft.com/en-us/help/305144/how-to-use-the-useraccountcontrol-flags-to-manipulate-user-account-pro
Get-DomainUser -Server \<DC\> -SPN -UACFilter DONT_EXPIRE_PASSWORD,NOT_ACCOUNTDISABLE -Properties samaccountname,distinguishedname,pwdlastset,memberof &#124; ? {$\_.Memberof -match "Domain Admins" -or $\_.Memberof -match "Enterprise Admins" -or $\_.Memberof -match "Administrators"} &#124; Export-Csv active_service_admin_pwd_non_exp.csv | Find active Service accounts within "Domain Admins", "Enterprise Admins" or "Administrators" with non-expiring password
Get-DomainUser -Domain DOMAIN -DomainController DC -UACFilter NOT_ACCOUNTDISABLE -Properties samaccountname | List all enabled domain users
Get-DomainUser -Domain DOMAIN -DomainController DC -UACFilter ACCOUNTDISABLE -Properties samaccountname | List all disabled domain users
Get-DomainComputer -UACFilter NOT\_ACCOUNTDISABLE &#124; Where-Object {($\_.operatingsystem -Like "\*Windows XP\*" -or $_.operatingsystem -Like "\*Server 2003\*" -or $\_.operatingsystem -Like "\*Server 2000\*" -or $\_.operatingsystem -Like "\*Windows 7\*" -or $\_.operatingsystem -Like "\*Vista\*" -or $\_.operatingsystem -Like "\*2008\*" -or $\_.operatingsystem -Like "\*8.1\*") -and $\_.lastlogontimestamp -gt (get-date).AddMonths(-6) } &#124; Select-Object samaccountname, distinguishedname, lastlogontimestamp, operatingsystem &#124; Export-Csv -Delimiter ";" obsolete_hosts.csv | Get a list of active (account not disabled and used in the last 6 months) Obsolete Operating Systems



Possible UACFilter values: 

SCRIPT, NOT_SCRIPT, ACCOUNTDISABLE, NOT_ACCOUNTDISABLE, HOMEDIR_REQUIRED, NOT_HOMEDIR_REQUIRED, LOCKOUT, NOT_LOCKOUT, PASSWD_NOTREQD, NOT_PASSWD_NOTREQD, PASSWD_CANT_CHANGE, NOT_PASSWD_CANT_CHANGE, ENCRYPTED_TEXT_PWD_ALLOWED, NOT_ENCRYPTED_TEXT_PWD_ALLOWED, TEMP_DUPLICATE_ACCOUNT, NOT_TEMP_DUPLICATE_ACCOUNT, NORMAL_ACCOUNT, NOT_NORMAL_ACCOUNT, INTERDOMAIN_TRUST_ACCOUNT, NOT_INTERDOMAIN_TRUST_ACCOUNT, WORKSTATION_TRUST_ACCOUNT, NOT_WORKSTATION_TRUST_ACCOUNT, SERVER_TRUST_ACCOUNT, NOT_SERVER_TRUST_ACCOUNT, DONT_EXPIRE_PASSWORD, NOT_DONT_EXPIRE_PASSWORD, MNS_LOGON_ACCOUNT, NOT_MNS_LOGON_ACCOUNT, SMARTCARD_REQUIRED, NOT_SMARTCARD_REQUIRED, TRUSTED_FOR_DELEGATION, NOT_TRUSTED_FOR_DELEGATION, NOT_DELEGATED, NOT_NOT_DELEGATED, USE_DES_KEY_ONLY, NOT_USE_DES_KEY_ONLY, DONT_REQ_PREAUTH, NOT_DONT_REQ_PREAUTH, PASSWORD_EXPIRED, NOT_PASSWORD_EXPIRED, TRUSTED_TO_AUTH_FOR_DELEGATION, NOT_TRUSTED_TO_AUTH_FOR_DELEGATION, PARTIAL_SECRETS_ACCOUNT, NOT_PARTIAL_SECRETS_ACCOUNT

### ACL ###

The AD module is required to use get-acl againt AD objects. 

Get the DACL of a domain object:

`(get-acl -path "AD:CN=BLAH,CN=BLAH,DC=BLAH,DC=LOCAL").access | export-csv acl.csv`

If the above command complains about AD "Cannot find drive. A drive with the name 'AD' does not exist.", you need to use AD for the first by for instance running "Get-ADDomainController".

For instance get the ACL of the domain object of a different domain from the current one:

```
New-PSDrive -name AD2 -PSProvider ActiveDirectory -server DOMAIN_FQDN -root "//RootDSE/"
(Get-ACL "AD2:$((Get-ADRootDSE -Server DOMAIN_FQDN).RootDomainNamingContext)").access | Export-Csv acl.csv
```

## Active Directory Module without installing RSAT

[ADModule](https://github.com/samratashok/ADModule)

`iex (new-Object Net.WebClient).DownloadString('https://raw.githubusercontent.com/samratashok/ADModule/master/Import-ActiveDirectory.ps1');Import-ActiveDirectory`




[Scanning for Active Directory Privileges & Privileged Accounts](https://adsecurity.org/?p=3658)


## Obtaining Owner from a non-domain-joined computer

It can be done with [DSACL](https://www.powershellgallery.com/packages/DSACL/0.2.0)

`dsacls.exe "\\DC_DNS_HOSTNAME\Object Distinguished name"`

For instance:

`dsacls.exe "\\DC001.domain.local\CN=SERVER01,OU=Server,DC=server,DC=local"`

## Tools for recon
Some of the tools that help with recon:

[Voyeur](https://github.com/silverhack/voyeur)

[BloodHound](https://github.com/BloodHoundAD/BloodHound)

[ADRecon](https://github.com/sense-of-security/ADRecon)

[Icebreaker](https://github.com/DanMcInerney/icebreaker)



## Protected Objects
https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/appendix-c--protected-accounts-and-groups-in-active-directory

**List users:**
RSAT

    Get-ADUser -LDAPFilter "(admincount=1)" | Select SamAccountName
 PowerView

     Get-DomainUser -AdminCount | Select SamAccountName

  
**List Groups:**
RSAT:

    Get-ADGroup -LDAPFilter "(admincount=1)" | Select SamAccountName

PowerView:
 
    Get-DomainGroup -AdminCount | Select SamAccountName
    
    

## Trusts

**List active trusts (trust account's password changed in the last 6 months) **

RSAT:

`foreach ($trust in Get-ADTrust -Filter * | Select-Object Direction,trustType,Target -Unique) { Get-ADObject ($trust.T
arget.Split(".")[0] + "$") | Where-Object pwdlastset -lt (Get-Date).AddMonths(-1) | Select-Object @{l="TrustTarget";e={$trust.Target}},samaccountname,@{l="Direction";e={$trust.Direction}},@{l=
"TrustType";e={$trust.TrustType}},WhenCreated,pwdlastset`


PowerView:

`foreach ($trust in Get-DomainTrust | Select-Object TrustType,TrustDirection,WhenCreated,TargetName -Unique) { Get-DomainObject ($trust.TargetName.Split(".")[0] + "$") | Where-Object pwdlastset -lt (Get-Date).AddMonths(-1) | Select-Object @{l="TrustTarget";e={$trust.TargetName}},samaccountname,@{l="Direction"
;e={$trust.TrustDirection}},@{l="TrustType";e={$trust.TrustType}},@{l="WhenCreated";e={$trust.WhenCreated}},pwdlastset }`


[adaudit - PowerShell Script to perform a quick AD audit](https://github.com/phillips321/adaudit)
