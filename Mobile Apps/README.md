[Mobile Roadmap](https://drive.google.com/file/d/1LoRA2aScPHkyEDWDnIK5D-ypYEXa_jIi/view)


iOS
=====

[Extracting the IPA File and Local Data Storage of an iOS Application](https://medium.com/@lucideus/extracting-the-ipa-file-and-local-data-storage-of-an-ios-application-be637745624d)

Bundle container of the app:

`/private/var/containers/Bundle/Application`

Data of the app:

`private/var/mobile/Containers/Data/Application/`


Read plist file:

`plistutil -i <FILE.plist>`


Install Clutch
-----------------
Instructions to install Clutch on iOS 12 with unc0ver jailbreak:

Build Clutch or download release from https://github.com/KJCracks/Clutch/releases

Then move binary to /usr/bin/Clutch

Then:

```
# safe place to work in
cd /private/var/mobile/Documents
# Get the ent from bash and save it
ldid -e `which bash` > ent.xml
# sign Clutch with the ent. "-Sent.xml" is the correct usage
ldid -Sent.xml `which Clutch`
# inject into trust cache
inject `which Clutch`
```

List installed apps

`Clutch -i`

Install Frida
---------------
https://www.frida.re/docs/ios/#with-jailbreak


Objection
------------
https://github.com/sensepost/objection

https://www.allysonomalley.com/2018/12/20/ios-pentesting-tools-part-3-frida-and-objection/
